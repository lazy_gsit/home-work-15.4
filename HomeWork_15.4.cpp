#include <iostream>
#include "EvenOdd.h"

using namespace std;

int main()
{
	PrintEvenOdd(true, 100);

	int range;
	cout << "Enter range: ";
	cin >> range;

	for (int i = 0; i < range; i++)
	{
		if (i % 2 == 0)
		{
			cout << i << endl;
		}
	}

}
